/* eslint-disable no-undef */
require('dotenv').config();
const { CONTRACT_NAMES } = require('../utils/constants');
const {
  readContract,
  writeContract,
  writeABI,
  readABI,
} = require('../utils/io');

task('deploy:grit404', 'Deploy GRIT404 Contract', async (_, { ethers }) => {
  const accounts = await ethers.getSigners();
  const signer = accounts[0];

  const grit404 = await ethers.deployContract(CONTRACT_NAMES.MY_ERC404, [
    "https://gritglobal.io",
    "GRIT",
    "GRIT",
    18,
    10000000,
    signer.address,
  ]);

  await grit404.waitForDeployment();

  console.info(`Contract deployed at ${grit404.target}`);

  writeContract(CONTRACT_NAMES.MY_ERC404, grit404.target, signer.address, []);
});

task('verify:grit404', 'Verify GRIT404 Contract', async (_, { run }) => {
  const grit404 = readContract(CONTRACT_NAMES.MY_ERC404);
  const accounts = await ethers.getSigners();
  const signer = accounts[0];
  console.log(run);
  await run('verify:verify', {
    address: grit404.address,
    constructorArguments: [
      // owner address
      signer.address,
    ],
  });
});

task(
  'set-data-uri',
  'Sets the DataURI in the contract',
  async (_, { ethers }) => {
    const grit404 = readContract(CONTRACT_NAMES.MY_ERC404);
    const abi = readABI(CONTRACT_NAMES.MY_ERC404);

    const accounts = await ethers.getSigners();
    const signer = accounts[0];

    const contract = new ethers.Contract(grit404.address, abi, signer);

    const dataURI = process.env.IMAGE_URL;

    const tx = await contract.setDataURI(dataURI);
    await tx.wait();

    console.info(`DataURI has been set successfully!`);
  },
);

task('abi:grit404', 'Export GRIT404 contract ABI', async () => {
  writeABI('GRIT404.sol/GRIT404.json', CONTRACT_NAMES.MY_ERC404);
});
