//SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

import {ERC404} from "./ERC404.sol";
import {Strings} from "@openzeppelin/contracts/utils/Strings.sol";

contract GRIT404 is ERC404 {
    using Strings for uint256;

    string public dataURI;
    string private baseTokenURI;
    constructor(string memory _dataURI, string memory _name, string memory _symbol, uint8 _decimals, uint256 _totalSupply, address _owner) ERC404(_name, _symbol, _decimals, _totalSupply, _owner) {
        dataURI = _dataURI;
        balanceOf[_owner] = _totalSupply * 10 ** _decimals;
        whitelist[_owner] = true;
    }

    function setDataURI(string memory _dataURI) public onlyOwner {
        dataURI = _dataURI;
    }

    function setTokenURI(string memory _tokenURI) public onlyOwner {
        baseTokenURI = _tokenURI;
    }

    function setNameSymbol(
        string memory _name,
        string memory _symbol
    ) public onlyOwner {
        _setNameSymbol(_name, _symbol);
    }

    function tokenURI(uint256 id) public view override returns (string memory) {
        if (bytes(baseTokenURI).length > 0) {
            return string.concat(baseTokenURI, Strings.toString(id));
        } else {
            string memory jsonPreImage = string.concat(
                string.concat(
                    string.concat('{"name": "',
                        string.concat(name,
                            string.concat('#', Strings.toString(id))
                        )
                    ),
                    '","description":"A collection of circles enabled by ERC404, an experimental token standard.","external_url":"","image":"'
                ),
                dataURI
            );
            string memory jsonPostImage = string.concat(
                '","attributes":[{"trait_type":"Color","value":"',
                'color'
            );
            string memory jsonPostTraits = '"}]}';

            return
                string.concat(
                "data:application/json;utf8,",
                string.concat(
                    string.concat(jsonPreImage, jsonPostImage),
                    jsonPostTraits
                )
            );
        }
    }
}
